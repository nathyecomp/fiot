/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fiot.learning.methods.neat;



/**
 *
 * @author Nathy
 */
public class Individuo implements Comparable<Individuo>{

    private Cromossomo cromossomo;
    private int fitness = 0;

    public Individuo(Cromossomo cromossomo) {
        this.cromossomo = cromossomo;
    }

    public Cromossomo getCromossomo() {
        return cromossomo;
    }

    public int getFitness() {
        return fitness;
    }
    
    public void setFitness(int fit){
        this.fitness = fit;
    }

    public void setCromossomo(Cromossomo cromossomo) {
        this.cromossomo = cromossomo;
    }

    @Override
     public int compareTo(Individuo other) {
       if (this.fitness < other.fitness) {
               return -1;
        }
       if (this.fitness > other.fitness) {
            return 1;
        }

        return 0;
  }
//    public double calcularFitness(World mapa) {
//        fitness = 0.0;
//        for (int cont = 0; cont < cromossomo.getGenes().size() - 1; cont++) {
//            fitness += mapa.getDistsEntrePontos()[cromossomo.getGene(cont)][cromossomo.getGene(cont + 1)];
//        }
//      return fitness;
//    }
}

