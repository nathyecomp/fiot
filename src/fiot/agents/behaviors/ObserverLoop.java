/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fiot.agents.behaviors;

import fiot.agents.AgentList;
import fiot.agents.ObserverAgent;
import fiot.agents.controller.Controller;
import fiot.agents.controller.ControllerList;
import fiot.agents.message.FIoTMessage;
import fiot.gui.PanelControl;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import fiot.learning.AdaptationMethod;
import fiot.learning.AdaptationMethodList;
import fiot.learning.Simulation;
import fiot.learning.methods.neat.Individuo;
import fiot.learning.methods.neat.NeatControl;

/**
 * Behavior of ObserverAgent
 * @author Nathalia
 * @version 1.0
 */
public abstract class ObserverLoop extends OneShotBehaviour {

    AgentList agentList;
    public ObserverAgent observerAgent;
    Simulation simulation;
    public String adressToAnswer = "";
    ControllerList listControl;
    
    
    //backpropagation
     double taxa = 0.1;
    double eq, eqm = 0.0;
    double eqm_atual = 1.0;
    double eqm_anterior = 987654321987654.0;
    double precisao;
    double yDesejado;
    double erro;
    double yFinal = 0.0;
    //AdaptationMethodList adaptList;
    // AdaptationMethod method;
    public ObserverLoop(Agent a) {
        super(a);
        observerAgent = (ObserverAgent) a;
        agentList = AgentList.getInstance();
        simulation = Simulation.getInstance();
        listControl = ControllerList.getInstance();
       /// adaptList = AdaptationMethodList.getInstance();
        // method = adaptList.getMethod(simulation.getMethodInUse());
    }
    

    @Override
    public void action() {
       // System.out.println("Executing OBSERVER LOOP");
        //  while (true) {
        if (simulation.getMethodInUse().equals("Genetic Algorithm")) {
           // System.out.println("OBSERVER LOOP: GENETIC ALGORITHM");
            try {
                if (simulation.isOnAdaptation()) {
                    this.trainingNeatControl();
                } else {
                    this.executingNeatControl();
                }
            } catch (FIPAException ex) {
                Logger.getLogger(ObserverLoop.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ObserverLoop.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // }
    }

    private void executingNeatControl() throws IOException, FIPAException {
        NeatControl neat;
        neat = NeatControl.getInstance();
        File f;
        f = new File("execution20032017.txt");
        FileWriter w = new FileWriter(f);
        BufferedWriter bw = new BufferedWriter(w);
        PrintWriter wr = new PrintWriter(bw);
        wr.append("INICIO: " + new Date(System.currentTimeMillis()) + "\r\n");
        String[] configs = simulation.getConfigurationExecution();
        double[] genesList;
        for (int cont = 0; cont < configs.length; cont++) {
            String[] genConfig = configs[cont].split(",");
            genesList = new double[genConfig.length];
            for (int cont2 = 0; cont2 < genConfig.length; cont2++) {
                DecimalFormat df = new DecimalFormat("#.#");

                String weight = df.format(Double.valueOf(genConfig[cont2]));
                weight = weight.replaceAll(",", ".");
                genesList[cont2] = Double.valueOf(weight);

                // System.out.println("gen "+ cont2+": "+genesList[cont2]);
            }

            NeatControl.getInstance().setGenes(genesList);
            this.listControl.getController(simulation.getControllerInUse()).change(genesList);
            double fitness = this.readResultSimulation();
            System.out.println("Fitness " + fitness);
            answerMessage();
        }

        wr.append("FIM: " + new Date(System.currentTimeMillis()) + "\r\n");

        wr.close();
        bw.close();

    }

    private void trainingBackPropagation(){
        
    }
    private void trainingNeatControl() throws FIPAException {
        FileWriter w2 = null;
        try {
            NeatControl neat;
            int numberOfTests = simulation.getNumOfSimulations();
            int numberOfGenerations = simulation.getNumOfGenerations();
            int numOfBestToBeSelected = simulation.getNumOfBestToBeSelected();
            int numOfChildrens = simulation.getNumOfChildrens();
            int numberOfPopulation = simulation.getNumberOfPopulation();
            int numExecutions = 0;          
            //elitism saving the fitness
            boolean saveFitness = simulation.isSaveFitness();
            neat = NeatControl.getInstance();
            // neat.setTimeSimulation(2);
            List<Individuo> listIndividuos;
            
            //to continue a simulation from file
            if(simulation.isFirstGenerationIsOn()){
                listIndividuos = new ArrayList<>(simulation.getFirstGeneration());
            }
            else{
                listIndividuos = NeatControl.getInstance().getFirstPopulation(numberOfPopulation);
            }
            List<Individuo> ordenadosPorFitness = new ArrayList<>();
            Individuo[] best = new Individuo[numberOfGenerations];
            Individuo[] worst = new Individuo[numberOfGenerations];
            File f;
            f = new File("evolutionSimulation20032017.txt");
            System.out.println("File for Evolution Simulation "+ f.getAbsolutePath());
            try {
                FileWriter w = new FileWriter(f);
                BufferedWriter bw = new BufferedWriter(w);
                PrintWriter wr = new PrintWriter(bw);
                wr.append("INICIO: " + new Date(System.currentTimeMillis()) + "\r\n");

                while (numExecutions < numberOfGenerations) {
                    ordenadosPorFitness = new ArrayList<>();
                    double average = 0;
                    System.out.println("Iniciando Geração: " + numExecutions);
                    int fitnessMax = 0;
                    int fitnessMin = 1000000000;
                    for (int cont = 0; cont < numberOfPopulation; cont++) {
                        Individuo indi = listIndividuos.get(cont);
                        double[] genes = indi.getCromossomo().getGenes();
                        NeatControl.getInstance().setGenes(genes);
                        this.listControl.getController(simulation.getControllerInUse()).change(genes);
                        int fitness;
                        if(saveFitness && indi.getFitness()>0){
                            fitness = indi.getFitness();
                        }
                        else{
                            int totalFitness = 0;
                            for (int test = 0; test < numberOfTests; test++) {
                                totalFitness += this.readResultSimulation();
                                answerMessage();
                            }
                            fitness= Math.round(totalFitness / numberOfTests);                          
                            indi.setFitness(fitness);
                            listIndividuos.get(cont).setFitness(fitness);//add p teste
                        }
                        //average of individuals of one generation
                        average += fitness;
//                        if (!ordenadosPorFitness.containsKey(fitness)) {
//                            List<Individuo> li = new ArrayList<>();
//                            ordenadosPorFitness.put(fitness, li);
//                        }
                        if (fitness >= fitnessMax) {
                            best[numExecutions] = indi;
                            fitnessMax = fitness;
                        }
                        if (fitness <= fitnessMin) {
                            worst[numExecutions] = indi;
                            fitnessMin = fitness;
                        }
                        //Select the best ones
                        //Os melhores são os de maior fitness
                      //  ordenadosPorFitness.get(fitness).add(indi);
                       
                        ordenadosPorFitness.add(cont,indi);

                    }
                    double divAverage = (average / numberOfPopulation);
                    ////Evolution
                    //   System.out.println("Cheguei aqui");
                     System.out.println("List before ord ");
                     this.printList(listIndividuos, numExecutions);
                     Collections.sort(listIndividuos);
                     
                     System.out.println("List after ord ");
                     this.printList(listIndividuos, numExecutions);
//test                    Collections.sort(ordenadosPorFitness);
                    

//                    System.out.println("Geração " + numExecutions + ":");
//                    System.out.println("Average: " + divAverage);
//                    System.out.println("Melhor fitness: " + best[numExecutions].getFitness());
//                    System.out.println("Genes: " + Arrays.toString(best[numExecutions].getCromossomo().getGenes()));
//                    System.out.println("Pior fitness: " + worst[numExecutions].getFitness());
//                    System.out.println("Genes: " + Arrays.toString(worst[numExecutions].getCromossomo().getGenes()));
                    //O melhor de cada geração
                    wr.append("Geração " + numExecutions + ":");
                    wr.append("Average: " + divAverage);
                    wr.append("Melhor fitness: " + best[numExecutions].getFitness());
                    wr.append("Genes: " + Arrays.toString(best[numExecutions].getCromossomo().getGenes()));
                    wr.append("Pior fitness: " + worst[numExecutions].getFitness());
                    wr.append("Genes: " + Arrays.toString(worst[numExecutions].getCromossomo().getGenes()));
                    wr.append("\n");
                    wr.flush();

                    //   if ((numExecutions - 1)==0||(numExecutions - 1)==1||(numExecutions - 1) == Math.round(numberOfGenerations / 10)||(numExecutions - 1) == Math.round(numberOfGenerations / 20)||(numExecutions - 1) == Math.round(numberOfGenerations / 6)||(numExecutions - 1) == Math.round(numberOfGenerations / 4)||(numExecutions - 1) == Math.round(numberOfGenerations / 2)) {
                       //save last generation on file
                    this.makeCopyGeneration(listIndividuos, (numExecutions));
//test                    this.makeCopyGeneration(ordenadosPorFitness, (numExecutions)); 
                    
                    numExecutions++;  
                    listIndividuos = NeatControl.getInstance().getNewPopulation(listIndividuos, numberOfPopulation, numOfBestToBeSelected);
                    
                    System.out.println("List created for New Generation");
                    this.printList(listIndividuos, numExecutions-1);
                    
                    System.out.println("-------------------END FOR GENERATION "+ (numExecutions-1)+" ------------------");
//test                    listIndividuos = NeatControl.getInstance().getNewPopulation(ordenadosPorFitness, numberOfPopulation, numOfBestToBeSelected);

                   // }
                }
                wr.append("Melhor da ultima geracao: ");
                wr.append("Geração " + (numExecutions - 1) + ":");
                wr.append("Melhor fitness: " + best[numExecutions - 1].getFitness());
                wr.append("Genes: " + Arrays.toString(best[numExecutions - 1].getCromossomo().getGenes()));

                double bestFitness = 0;
                Individuo bestIndividuo = null;
                int ger = 0;
                int bestGer = 0;
                System.out.println("Final do arquivo");
                for(int contIndi = 0; contIndi<best.length;contIndi++){
                //for (Individuo best1 : best) {
                    Individuo best1 = best[contIndi];
                    double fitBest = best1.getFitness();
                    System.out.println(contIndi+": ");
                    System.out.println("ger: " + ger);
                    System.out.println("Fitness "+ fitBest);
                    if (fitBest >= bestFitness) {
                        bestIndividuo = best1;
                        bestFitness = fitBest;
                        bestGer = ger;
                    }
                    ger++;
                //}
                }
                wr.append("Melhor de todas as gerações: ");
                wr.append("Geração " + bestGer + ":");
                wr.append("Fitness: " + bestIndividuo.getFitness());
                wr.append("Genes: " + Arrays.toString(bestIndividuo.getCromossomo().getGenes()));

                double worstFitness = 1000000000;
                Individuo worstIndividuo = null;
                ger = 0;
                int worstGer = 0;
                for (Individuo worst1 : worst) {
                    double fitWorst = worst1.getFitness();
                    if (fitWorst <= worstFitness) {
                        worstIndividuo = worst1;
                        worstFitness = fitWorst;
                        worstGer = ger;
                    }
                    ger++;
                }
                wr.append("Pior de todas as gerações: ");
                wr.append("Geração " + worstGer + ":");
                wr.append("Fitness: " + worstIndividuo.getFitness());
                wr.append("Genes: " + Arrays.toString(worstIndividuo.getCromossomo().getGenes()));

                wr.append("FIM: " + new Date(System.currentTimeMillis()) + "\r\n");

                wr.close();
                bw.close();
                
                //save last generation on file
                //this.makeCopyGeneration(ordenadosPorFitness, ger);
            } catch (IOException e) {
                System.out.println("Error" + e);
            }
            
        }  finally {
           
        }
    }
    
//   public int readResultSimulationTest(){
//      return (int) (Math.random() * this.simulation.getNumberOfPopulation());
//   }
    //NEED TO BE ADAPT - depends on application     
    public abstract double readResultSimulation() throws FIPAException;
   

    private void answerMessage() throws FIPAException {
        //"initSimulation"
        // System.out.println("OBSERVER LOOP: ANSWER MESSAGE");
        FIoTMessage msg = new FIoTMessage();
        msg.setContent("initSimulation");
        msg.setIdSender(adressToAnswer);
        this.observerAgent.getMsgController().sendFIoTMessage(msg);
       // this.observerAgent.sentMsgToSelectRecipient(this.adressToAnswer, "initSimulation");
    }
    
    
    private void printList(List<Individuo> list, int generation){
        if (list != null && !list.isEmpty()) {
                int contI = 0;
                    for (Individuo indi : list) {
                        System.out.println("Indi " + contI + ": ");
                        System.out.println("Fitness: " + indi.getFitness());
                        System.out.println("Genotype: " + Arrays.toString(indi.getCromossomo().getGenes()));
                        System.out.println("\n");
                        contI++;
                    }
                
            }
    }
    
    private void makeCopyGeneration(List<Individuo> list, int generation){
        FileWriter w2 = null;
        try {
            File f2 = new File("3105Generation" + generation+ ".txt");
            w2 = new FileWriter(f2);
            BufferedWriter bw2 = new BufferedWriter(w2);
            PrintWriter wr2 = new PrintWriter(bw2);
            wr2.append("INICIO: " + new Date(System.currentTimeMillis()) + "\r\n");
            wr2.append("INDIVIDUALS OF LAST GENERATION: " + generation);
            wr2.append("\n");
            if (list != null && !list.isEmpty()) {
                int contI = 0;
                    for (Individuo indi : list) {
                        wr2.append("Indi " + contI + ": ");
                        wr2.append("Fitness: " + indi.getFitness());
                        wr2.append("Genotype: " + Arrays.toString(indi.getCromossomo().getGenes()));
                        wr2.append("\n");
                        contI++;
                    }
                
            }
            
            wr2.close();
            bw2.close();
        } catch (IOException ex) {
            Logger.getLogger(ObserverLoop.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                w2.close();
            } catch (IOException ex) {
                Logger.getLogger(ObserverLoop.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    /*
     private int execute() {

     world.setFinishSimulation(false);
     int aa = 0;
     while(aa++<200){
            
     }
     int time = 20;
     int cont = 0;
     int numCars = 0;
     int numCarsOrigin = world.getNumCars();
     double fitness = 0;
     Map<String, Car> listCar = null;
     while (cont++ < time) {
     world.setActualTimeSimulation(cont);
     listCar = world.getListCar();
     //            System.out.println("Cont eh "+ cont);
     //            System.out.println("Tamanho atual "+listCar.size());
     List<Car> list = new ArrayList<>(listCar.values());
     for (Car car : list) {
     if (car.getActualRoad().equals(car.getRoadEnd())) {
     world.removeCar(car.getId());
     numCars++;
     fitness += cont;
     } //                else{
     //                    System.out.println("Carro "+ car.getId());
     //                    System.out.println("Esta em "+car.getActualRoad());
     //                    System.out.println("Quer ir para "+ car.getNextRoute());
     //                    Road ro = (Road)world.getElement(car.getNextRoute());
     //                    System.out.println("Só que nextRoute tem "+ro.getNumCar());
     //                }
     else {
     String dest = scenario.getNextRoad(car.getId());
     world.changeRoadCar(car.getActualRoad(), dest, car.getId());
     }
     }
     }

     int fit = (int) ((numCars * 10) - (fitness/15));
     world.setFinishSimulation(true);
     System.out.println("O numero total de carros eh " + world.getNumCars());
     System.out.println("O numero de carros que terminaram " + numCars);
     System.out.println("Tempo de viagem dos que completaram " + fitness);
     System.out.println("Fitness: "+fit);
     //Quanto maior o fitness, é porque demorou muito pra chegar
     return fit;
     }
     */

}
