/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fiot.agents;

import fiot.agents.behaviors.CreateNewAdaptiveAgent;
import fiot.agents.behaviors.ObserverLoop;
import fiot.agents.message.AgentAdress;
import instance.agent.behaviors.ObserverLoop_StreetLight;
import jade.core.AID;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author Nathalia
 */
public class ObserverAgent extends FIoTAgent{

	private static final long serialVersionUID = -2120102533050279597L;

    public ObserverAgent(String typeControllerMsg, AgentAdress adress) {
        super(typeControllerMsg, adress);
    }

    protected void setup() {

        super.setup();
        System.out.println("Agent started: " + this.getAID());
        this.create();
        // this.controlLoop = buildControlLoop();
        addBehaviour(new ObserverLoop_StreetLight(this));
    }
    
    public void create(){
        AgentList.getInstance().setObserverAgent(this);
    }

//   public ACLMessage receiveMessage() {
//        ACLMessage msg = receive();
//        return msg;
//    } 
    
//    public String[] getSenderIP(ACLMessage msg){
//        return msg.getSender().getAddressesArray();
//    }
    
//    public void sentMsgToSelectRecipient(String nameAgent, String content) throws FIPAException {
//        AMSAgentDescription[] agents = null;
//
//        SearchConstraints c = new SearchConstraints();
//        c.setMaxResults(new Long(-1));
//        agents = AMSService.search(this, new AMSAgentDescription(), c);
//        AID myID = getAID();
//        for (int i = 0; i < agents.length; i++) {
//            AID agentID = agents[i].getName();
//            // System.out.println("Name agent eh "+agentID.getLocalName());
//            if (agentID.getLocalName().equals(nameAgent)) {
//                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
//                msg.setContent(content);
//                msg.addReceiver(agents[i].getName());
//                send(msg);
//            }
//        }
//
//    }
}
