/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fiot.agents;

import fiot.agents.behaviors.ControlLoop;
import fiot.agents.controller.Controller;
import fiot.agents.device.Device;
import fiot.agents.message.AgentAdress;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import java.io.Serializable;

/**
 *AdapgetTempive AgengetTemp class
 * @author NagetTemphalia
 * @version 1.0
 */
public class AdaptiveAgent extends FIoTAgent {

    private static final long serialVersionUID = -2120102533050279597L;
    private Device device;
    //private Controller controller;
    private ControlLoop controlLoop;

    String nameAgent, nameContainer;
    private AID aid;
    private Serializable messageReceived;
    private Serializable messageToSend = null;

    public AdaptiveAgent(Device device, String typeControllerMsg, AgentAdress adress) {
        //   setNameAgent(name);
          // setNameContainer(container);
        super(typeControllerMsg,adress);
           this.device = device;   
    }
    
    public void create(){
        AgentList.getInstance().addAgent(this.getName(), this);
    }
    protected void setup() {

        super.setup();
        System.out.println("Agent started: " + this.getAID());
        this.create();
       // this.controlLoop = buildControlLoop();
        addBehaviour(new ControlLoop(this));
    }
    
    public double[] getInput(){
      return  this.device.getInputValue();
    }
    
    public void setInput(double[] input){
        this.device.setInputValue(input);
    }
    
    public double[] getOutput(){
        return this.device.getOutputValue();
    }
    
    public void setOutput(double[] outputValue){
        this.device.setOutputValue(outputValue);
    }

    public String getNameAgent() {
        return nameAgent;
    }

    public void setNameAgent(String nameAgent) {
        this.nameAgent = nameAgent;
    }

    public String getNameContainer() {
        return nameContainer;
    }

    public void setNameContainer(String nameContainer) {
        this.nameContainer = nameContainer;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

//    public Controller getController() {
//        return controller;
//    }
//
//    public void setController(Controller controller) {
//        this.controller = controller;
//    }

    public ControlLoop getControlLoop() {
        return controlLoop;
    }

    public void setControlLoop(ControlLoop controlLoop) {
        this.controlLoop = controlLoop;
    }

//    public AID getAid() {
//        return aid;
//    }

//    public void setAid(AID aid) {
//        this.aid = aid;
//    }

    
//    public void replyMessage(ACLMessage msg, String contentReply) {
//        ACLMessage reply = new ACLMessage(ACLMessage.INFORM);
//        reply.setContent(contentReply);
//        reply.addReceiver(msg.getSender());
//        send(reply);
//    }

//    public ACLMessage receiveMessage() {
//        ACLMessage msg = receive();
//        return msg;
//    } 
    
//    public String[] getSenderIP(ACLMessage msg){
//        return msg.getSender().getAddressesArray();
//    }

//    public void sendMessage(String content, String containerAgent) {
//        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
//        msg.setContent(content);
//        //msg.addReceiver( new AID( containerAgent, AID.ISLOCALNAME) );
//        msg.addReceiver(this.aid);
//        send(msg);
//    }

//    public void sentMsgToSelectRecipient(String nameAgent, String content) throws FIPAException {
//        AMSAgentDescription[] agents = null;
//
//        SearchConstraints c = new SearchConstraints();
//        c.setMaxResults(new Long(-1));
//        agents = AMSService.search(this, new AMSAgentDescription(), c);
//        AID myID = getAID();
//        for (int i = 0; i < agents.length; i++) {
//            AID agentID = agents[i].getName();
//            // System.out.println("Name agent eh "+agentID.getLocalName());
//            if (agentID.getLocalName().equals(nameAgent)) {
//                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
//                msg.setContent(content);
//                msg.addReceiver(agents[i].getName());
//                send(msg);
//            }
//        }
//
//    }
    
    
}
